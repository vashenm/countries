import './App.css';
import CountryList from './Components/CountryList/CountryList';

function App() {
  return (
    <div className="App">
      <CountryList></CountryList>
    </div>
  );
}

export default App;
